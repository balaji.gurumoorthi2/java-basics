import java.util.Scanner;
public class BMICalculator {
   public static void main(String args[]) {
      Scanner sc = new Scanner(System.in);
      System.out.print("Input weight in kilogram: ");
      float weight = sc.nextFloat();
      System.out.print("\nInput height in meters: ");
      float height = sc.nextFloat();
      float BMI = weight / (height * height);
      System.out.print("\nThe Body Mass Index (BMI) is " + BMI + " kg/m2");
      
      if(BMI>25)
    	  System.out.println("\nYou are Overweight");
      else if(BMI<20)
    	  System.out.println("\nYou are Underweight");
      else 
    	  System.out.println("\nYou are Normal");
      
      sc.close();
   }
}