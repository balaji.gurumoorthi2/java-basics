
public class Grid {

	public static void main(String[] args) {
		int[][] grid = new int[4][4];
		int odd=1;
		int even=2;
		for(int i=0;i<4;i++)
		{
			if(i%2==0) 
			{   
				for(int j=0;j<4;j++)
				{
					grid[i][j]=odd;
					odd+=2;
					System.out.print(grid[i][j]+"\t");
				}
			}
			else
			{
				for(int j=0;j<4;j++)
				{
					grid[i][j]=even;
					even+=2;
					System.out.print(grid[i][j]+"\t");				}
			}
		System.out.print("\n");
		}		
		
	}
}
